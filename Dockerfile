FROM python:3.10-alpine
WORKDIR /work
ENV FLASK_APP=fhost.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers alpine-sdk
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask","run"]
